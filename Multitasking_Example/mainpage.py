##
# @mainpage
# @section      sec_main_intro Introduction
#               This example project shows simple methods for cooperative
#               multi-tasking.
#
# @section      sec_main_tasks Tasks
#               This project implements a round-robin cooperative multi-tasking
#               scheme. More details can be found on the \ref page_tasks page.
#
#               The following tasks are implemented:
#               - @ref page_task_1 "Example Task #1"
#               - @ref page_task_2 "Example Task #2"
#               - @ref page_task_3 "Example Task #3"
#
#
# @author       Your name
# @copyright    License Info
# @date         January 1, 1970