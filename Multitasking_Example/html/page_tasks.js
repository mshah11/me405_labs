var page_tasks =
[
    [ "Task 1", "page_task_1.html", [
      [ "Introduction", "page_task_1.html#sec_task_1_intro", null ],
      [ "Finite State Machine", "page_task_1.html#sec_task_1_fsm", [
        [ "State Transition Diagram", "page_task_1.html#sec_task_1_trans", null ]
      ] ],
      [ "Implementation", "page_task_1.html#sec_task_1_imp", null ]
    ] ],
    [ "Task 2", "page_task_2.html", [
      [ "Introduction", "page_task_2.html#sec_task_2_intro", null ],
      [ "Finite State Machine", "page_task_2.html#sec_task_2_fsm", [
        [ "State Transition Diagram", "page_task_2.html#sec_task_2_trans", null ]
      ] ],
      [ "Implementation", "page_task_2.html#sec_task_2_imp", null ]
    ] ],
    [ "Task 3", "page_task_3.html", [
      [ "Introduction", "page_task_3.html#sec_task_3_intro", null ],
      [ "Finite State Machine", "page_task_3.html#sec_task_3_fsm", [
        [ "State Transition Diagram", "page_task_3.html#sec_task_3_trans", null ]
      ] ],
      [ "Implementation", "page_task_3.html#sec_task_3_imp", null ]
    ] ]
];