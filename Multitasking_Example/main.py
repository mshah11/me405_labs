'''
@file       main.py
@brief      Main Script File
@details    Main script file that runs continuously. The only thing that
            happens in this file is initializing tasks and then idefinitely
            running the task loop at a fixed interval. This could be considered
            a "round-robin" scheduler, which is a cooperative multi-tasking
            scheme.
            
            Each task should be specified in a seperate file  using a Python 
            class called. The class must contain a function called @c run that
            performs one single iteration of the task.
'''

import utime
from task_1 import Task_1
from task_2 import Task_2
from task_3 import Task_3

## 
# @brief    List of task classes
# @details  This Python list contains all of the objects representing the
#           different tasks that will be called during the task loop.
#
task_list = [Task_1(), Task_2(), Task_3()]

## @brief   Task loop rate in uS
#  @details A Python integer that represents the nominal number of microseconds
#           between iterations of the task loop.
#
interval = 10000

## @brief   The timestamp of the next iteration of the task loop
#  @details A value from utime.ticks_us() specifying the timestamp for the next
#           iteration of the task loop.
#
#           This value is updated every time the task loop runs to schedule the
#           next iteration of the task loop.
#
next_time = utime.ticks_add(utime.ticks_us(), interval)

## @brief   The timestamp associated with the current iteration of the task
#           loop
#  @details A value from utime.ticks_us() updated every time the task loop 
#           checks to see if it needs to run. As soon as this timestamp exceeds
#           next_time the task loop runs an iteration.
#
cur_time = utime.ticks_us()

while True:
    # Update the current timestamp
    cur_time = utime.ticks_us()
    
    # Check if its time to run the tasks yet
    if utime.ticks_diff(cur_time, next_time) > 0:
        
        # If it is time to run, update the timestamp for the next iteration and
        # then run each task in the task list. 
        next_time = utime.ticks_add(next_time, interval)
        
        for task in task_list:
            task.run()