'''@file IntegralController.py

@package IntegralController

@subsection Description
This class defines a ProportionalController object for closed loop motor control.
It utilizes a proportional gain to amplify the error betweem the referenced and desired position.
'''
import pyb

class IntegralController:
    # This class impements an Proportional Controller 
    # for the ME 405 board.
    
    def __init__(self, k_i):
        ''' Creates a proportional controller object by initializing 
        pins.
        @param kp      A numerical value representing the integral gain.'''
        
        self.ki = k_i
        self.val1 = 0
        self.val2 = 0
        self.total_err = 0
        
        print('Creating an Integral Controller with K_i = ', k_i)
        
                                                       
    def update(self, ref, meas):
        ''' Updates the controller with the reference and measured values.
        Returns the error.
        @param ref     Reference value for the motor position.
        @param meas    Measured motor position'''
        
        # Returns the actuation value (kp*error)
        err = ref-meas
        self.total_err += err
        
        if err == 0:
            self.total_err = 0
        
        
        
        return self.ki * self.total_err
    
    
