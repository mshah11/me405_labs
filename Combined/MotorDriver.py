## @file MotorDriver.py
#
#  @package MotorDriver
# This program implements a MotorDriver class and tests it.
# Link to the source code: https://bitbucket.org/mshah11/me405_labs/src/master/lab_1/

#
# @author Mihir Shah
#
# @copyright Mihir Shah, 2020
#
# @date 4/24/2020
#
#


import pyb

class MotorDriver:
    # This class implements a motor driver for the
    # ME405 board.
    
    def __init__ (self, EN_pin, IN1_pin, IN2_pin, timer):
        ''' Creates a motor driver by initializing GPIO
        pins and turning the motor off for safety.
        @param EN_pin  A pyb.Pin object to use as the enable pin.
        @param IN1_pin A pyb.Pin object to use as the input to half bridge 1.
        @param IN2_pin A pyb.Pin object to use as the input to half bridge 2.
        @param timer   A pyb.Timer object to use for PWM generation on IN1_pin
                       and IN2_pin. '''
        print ('Creating a motor driver')
        self.EN_pin = EN_pin
        self.EN_pin.low()
        self.IN1_pin = IN1_pin
        self.IN2_pin = IN2_pin
        self.timer = timer
        self.tch1 = self.timer.channel(1, pyb.Timer.PWM, pin=self.IN1_pin)
        self.tch2 = self.timer.channel(2, pyb.Timer.PWM, pin=self.IN2_pin)
        
        
    def enable (self):
        ''' This method enables the motor to run.'''
        print ('Enabling Motor')
        self.EN_pin.high()

    def disable (self):
        ''' This method disables the motor from running.'''
        print ('Disabling Motor')
        self.EN_pin.low()
        
    def set_duty (self, duty):
        ''' This method sets the duty cycle to be sent
        to the motor to the given level. Positive values
        cause effort in one direction, negative values
        in the opposite direction.
        @param duty A signed integer holding the duty
        cycle of the PWM signal sent to the motor '''
        
        
        if duty > 100:
            # saturation scheme for duty input
            duty = 100
            print ('Input Too Big. Saturated to 100%')
            
        elif duty < -100:
            duty = -100
            print ('Input Too Small. Saturated to -100%')
            
        if duty >= 0:
            print ('Setting Duty Cycle to {:d}'.format(duty) + '%')
            # positive duty input
            self.tch2.pulse_width_percent(0)
            self.tch1.pulse_width_percent(duty)
        
        else:
            # negative duty input
            print ('Setting Duty Cycle to {:d}'.format(duty) + '%')
            self.tch1.pulse_width_percent(0)
            self.tch2.pulse_width_percent(-duty)
        
    

if __name__ == '__main__':
    # Code used to test the MotorDriver class
    
    # Create the pin objects used for interfacing with the motor driver
    pin_EN = pyb.Pin(pyb.Pin.cpu.A10, pyb.Pin.OUT_PP)
    pin_IN1 = pyb.Pin(pyb.Pin.cpu.B4)
    pin_IN2 = pyb.Pin(pyb.Pin.cpu.B5)
    
    # Create the timer object used for PWM generation
    tim = pyb.Timer(3, freq=20000)
    
    # Create a motor object passing in the pins and timer
    moe = MotorDriver(pin_EN, pin_IN1, pin_IN2, tim)
    # Enable the motor driver
    moe.enable()
    
    # Set the duty cycle to 10 percent
    moe.set_duty(10)
