''' @file mainpage.py

@author Mihir Shah

@mainpage

@section intro Introduction
This landing page is for documentation of code Mihir Shah has generated for ME 405.

@section classes Class Files
\li MotorDriver
\li EncoderDriver
\li ProportionalController
\li IMUDriver

@section tests User Test Files
\li StepResponseTest

@section Code
\li Lab source can be found here: https://bitbucket.org/mshah11/me405_labs/src/master/

@author Mihir Shah
@copyright Mihir Shah 2020
@date May 14, 2020  


@page StepResponseDoc Step Response Test Process

@section Description
This showcases the step response testing process to tune the Kp for this controller.
I had 2 goals in tuning this controller:
    First, to have minimal overshoot.
    Second, to have the quickest response time.
    
In order to tune this controller I started with a Kp of 1, then adjusted from there.

@section Plots

@image html step1.PNG

This was the first step response plot. I used a Kp of 1 and setpoint of 500 and 
did not know what to expect. These were the results:
    
Setpoint =  500
Final Position =  497
Error =  3
Error % =  0.6

There was plenty of overshoot so for the next trial I 
reduced the Kp.

@image html step2.PNG

This was the second step response plot. I used a Kp of 0.7 and setpoint of 500.
These were the results:
Setpoint =  500
Final Position =  494
Error =  6
Error % =  1.2

Since there was still overshoot I drastically reduced the Kp to 0.1 for Trial 3

@image html step3.PNG

Finally I achieved a step response with no overshoot. These were the results:
Setpoint =  500
Final Position =  457
Error =  43
Error % =  8.6

The response video can be found here: https://cpslo-my.sharepoint.com/:v:/g/personal/mshah11_calpoly_edu/ESWh2bD56WBEpHo6g6LqYeoBoAHFZXRkV1aZvLKt3pD8ng?e=SyZLfS
Something to note: The final position always ends up with a small error. This 
is due to the stiction required for the motor to start spinning. With this controller,
the actuation value becomes too small to actually turn the motor once the error is very
small. Thus, the final position has a small error compared to the desired value.    


@page IMUTestDoc IMU Test Process

@section Description
This section shows the process of testing the IMU with IMUDriver to ensure that Euler angles were correctly updating.
Calibrating is very simple in the NDOF mode, just turning the IMU around calibrates it.
To test the calibration, I set the IMU on top of a protractor and the rotated it in the x, y , and z planes.
Seeing the values change on screen told me whether the code was working as it should be.

@image html IMU1.jpg

This image shows the testing setup.

The testing video can be found here: https://cpslo-my.sharepoint.com/:v:/g/personal/mshah11_calpoly_edu/EXCEp_Wd-7lHlKVvSFhb9k4BPxvnljfZV9B2m9HyKcrzjw?e=UKNcG2

Source code can be found here: https://bitbucket.org/mshah11/me405_labs/src/master/lab_4/
























@page projectproposal Term Project Proposal

@section pstate Problem Statement
Two wheeled transport vehicles have a hard time balancing on their own, especially when the two wheels are parallel to each other.
This project will attempt to create a closed loop algorithm to allow balancing of the vehicle using only the motors.

@section req Requirements
- The vehicle must remain steady without outside assistance.
- The vehicle must counter tipping up to 15 degrees from outside forces.
- The vehicle must not spin or otherwise move more than a few inches when stationary.

@section mat Materials
I anticipate needing only some LEGO pieces, household tools, adhesives, and the ME 405 kit.

@section mats Material Access
I have access to all the materials I anticipate needing for this project.

@section bom Bill Of Materials
- None as of now.

@section safe Safety Assessment
I will have to make sure:
- I don't hurt myself if I use a soldering iron.
- I don't step on a LEGO, which could cause great pain.
- the device can easily be switched off to prevent hair, fingers, or other body parts from being tangled in gearing.

@section time Timeline
- Week 7: Project Proposal
- Week 8: Basic physical protoype, begin working on cod
- Week 9: Code done, physical prototype functional
- Week 10 (Finals week): Physical model fully functional, code fully documented and uploaded.
'''