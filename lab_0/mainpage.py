## @file mainpage.py
#  Brief doc for mainpage.py
#
#  Detailed doc for mainpage.py 
#
#  @mainpage
#
#  @section sec_intro Introduction
#  This is the landing page for Lab 0. Lab 0 was a Fibonacci Sequence computer.
#  The code asks the user to input the index of the Fibonacci number they would
#  like to calculate. The program then outputs the Fibonacci number using a 
#  recursive method.
#
#  @author Mihir Shah
#
#  @copyright none
#
#  @date April 17, 2020
#