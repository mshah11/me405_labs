## @file main.py
# This file contains code that calculates Fibonacci numbers. Adapted from my own AP Computer Science code '''
def fib (idx):
# This method calculates a Fibonacci number corresponding to 
#   a specified index.
#   @param idx An integer specifying the index of the desired
#              Fibonacci number.
    
# this is my algorithm for the Fibonacci calculator.   
# it uses recursion to find the Fib value at the given index.
               
    if (idx == 0):
        return 0
    elif(idx == 1):
        return 1
    else:
        return fib(idx - 1) + fib(idx - 2)
    
# User Interface section.    
if __name__ == '__main__':
    # Asks use whether they want to run the program.
    userinput = input('Do you want to enter a Fibonacci index? '
                      '[y for yes, anything else to exit]: ')
    # runs a while loop while input is appropriate 
    while userinput == 'y':
        idx = input('What index would you like to find the Fibonacci'
                    ' number for? [any positive integer]: ')
        # ensures that the input is valid before proceeding
        if idx.isdigit():
            print ('Calculating Fibonacci number at '
           'index n = {:}.'.format(idx))
            print ('The Fibonacci number is {:}.'.format(fib(int(idx))))    
            userinput = input('Do you want to enter a Fibonacci index? '
                              '[y for yes, anything else to exit]: ')
        else:
            print ('Invalid input. Please try again.')
    # Good UI        
    print ('Program terminated. We hope to see you again soon!')