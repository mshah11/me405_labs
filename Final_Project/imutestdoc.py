# @page IMUTestDoc IMU Test Process

# @section sec_description Description
# This section shows the process of testing the IMU with IMUDriver to ensure that Euler angles were correctly updating.
# Calibrating is very simple in the NDOF mode, just turning the IMU around calibrates it.
# To test the calibration, I set the IMU on top of a protractor and the rotated it in the x, y , and z planes.
# Seeing the values change on screen told me whether the code was working as it should be.

# @image html IMU1.jpg

# This image shows the testing setup.

# The testing video can be found here: https://cpslo-my.sharepoint.com/:v:/g/personal/mshah11_calpoly_edu/EXCEp_Wd-7lHlKVvSFhb9k4BPxvnljfZV9B2m9HyKcrzjw?e=UKNcG2

# Source code can be found here: https://bitbucket.org
