''' @file        finalprojectscript.py
 
 @page         finalprojectscript Term Project Script

 @section      sec_script_intro Introduction
               This script shows closed loop control of the balance bot. 
               It uses the gyroscope to control the motors.
               

 @section      sec_script_snip Pertinent Code Snippets



This code creates all the objects that will be used. Note that 2 motor objects are being separately created
 @code{.py}

    # Create the pin objects used for interfacing with the motor driver
    pin_ENa = pyb.Pin(pyb.Pin.cpu.A10, pyb.Pin.OUT_PP)
    pin_IN1a = pyb.Pin(pyb.Pin.cpu.B4)
    pin_IN2a = pyb.Pin(pyb.Pin.cpu.B5)
    
    pin_ENb = pyb.Pin(pyb.Pin.cpu.C1, pyb.Pin.OUT_PP)
    pin_IN1b = pyb.Pin(pyb.Pin.cpu.A0)
    pin_IN2b = pyb.Pin(pyb.Pin.cpu.A1)
    
    # Create the timer objects used for PWM generation
    tima = pyb.Timer(3, freq=20000)
    timb = pyb.Timer(5, freq=20000)
    
    # Create the motor objects passing in the pins and timer
    moea = MotorDriver(pin_ENa, pin_IN1a, pin_IN2a, tima)
    moeb = MotorDriver(pin_ENb, pin_IN1b, pin_IN2b, timb)

@endcode

Then the motors and IMU were enabled. The IMU is set to NDOF mode.

@code{.py}

    # Enable the motor drivers
    moea.enable()
    moeb.enable()
    
    # Create IMUDriver and enable it, setting mode to NDOF
    boi = IMUDriver(40)
    boi.enable()
    boi.set_mode(0b1100)

@endcode

The last setup step is to set the Proportional and Integral controller gains.
It turned out that the Integral controller was not very useful so it is set to 0.

@code{.py}

    # Create ProportionalController and IntegralController with K values
    proppy = ProportionalController(.5)
    inty = IntegralController(0)

@endcode

Finally, enter the infinite loop. A few different things happen within this loop.
However, since I want the response to be as quick as possible there is no delay or round-robin scheduler.

@code{.py}

    # infinite loop
    i = 1000
    while i > 0:
        
        # Get the Euler angles from gyroscope
        bro = boi.get_Euler_tuple()
        
        # Set the measured degree to Euler angle we care about.
        meas_deg = bro[1]
        
        # Set reference value to 0 degrees
        ref = 0
            
        # Determine Proportional and Integral signals
        Prop_err = proppy.update(ref, meas_deg)
        Integ_err = inty.update(ref, meas_deg)
        
        # Set the new duty cycle input value 
        dootie = -int(Prop_err + Integ_err)
        
        # Removing motor deadband
        if dootie > 1:
            dootie+=15
        elif dootie < -1:
            dootie-=15
        else:
            dootie = 0   
            
        # Set duty for each motor as simultaneously as possible
        moea.set_duty(dootie)
        moeb.set_duty(dootie)
        
        # Do it all again.

@endcode

@section        sec_script_Conclusion Conclusion

This script seems robust and easily modifiable, with the 2 most important parameters
being Proportional and Integral gains. Maybe this script could be converted into a class
of its own, allowing for a BalanceBot object. For more information on this project please
visit the \ref FinalProjectTest page.

To see the source code in its entirety, visit https://bitbucket.org/mshah11/me405_labs/src/master/Final_Project/


'''
import pyb
import utime
from MotorDriver import MotorDriver
from EncoderDriver import EncoderDriver
from IMUDriver import IMUDriver
from ProportionalController import ProportionalController
from IntegralController import IntegralController


if __name__ == '__main__':
    
    
    
    
    # Create the pin objects used for interfacing with the motor driver
    pin_ENa = pyb.Pin(pyb.Pin.cpu.A10, pyb.Pin.OUT_PP)
    pin_IN1a = pyb.Pin(pyb.Pin.cpu.B4)
    pin_IN2a = pyb.Pin(pyb.Pin.cpu.B5)
    
    pin_ENb = pyb.Pin(pyb.Pin.cpu.C1, pyb.Pin.OUT_PP)
    pin_IN1b = pyb.Pin(pyb.Pin.cpu.A0)
    pin_IN2b = pyb.Pin(pyb.Pin.cpu.A1)
    
    # Create the timer objects used for PWM generation
    tima = pyb.Timer(3, freq=20000)
    timb = pyb.Timer(5, freq=20000)
    
    # Create the motor objects passing in the pins and timer
    moea = MotorDriver(pin_ENa, pin_IN1a, pin_IN2a, tima)
    moeb = MotorDriver(pin_ENb, pin_IN1b, pin_IN2b, timb)
    
    # Enable the motor drivers
    moea.enable()
    moeb.enable()
    
    # Create IMUDriver and enable it, setting mode to NDOF
    boi = IMUDriver(40)
    boi.enable()
    boi.set_mode(0b1100)
    
    # Create ProportionalController and IntegralController with K values
    proppy = ProportionalController(.5)
    inty = IntegralController(0)
    
    # infinite loop
    i = 1000
    while i > 0:
        
        # Get the Euler angles from gyroscope
        bro = boi.get_Euler_tuple()
        
        # Set the measured degree to Euler angle we care about.
        meas_deg = bro[1]
        
        # Set reference value to 0 degrees
        ref = 0
            
        # Determine Proportional and Integral signals
        Prop_err = proppy.update(ref, meas_deg)
        Integ_err = inty.update(ref, meas_deg)
        
        # Set the new duty cycle input value 
        dootie = -int(Prop_err + Integ_err)
        
        # Removing motor deadband
        if dootie > 1:
            dootie+=15
        elif dootie < -1:
            dootie-=15
        else:
            dootie = 0   
            
        # Set duty for each motor as simultaneously as possible
        moea.set_duty(dootie)
        moeb.set_duty(dootie)
        
        # Do it all again.
