'''

@file ProportionalController.py

@package ProportionalController

@subsection Description
This class defines a ProportionalController object for closed loop motor control.
It utilizes a proportional gain to amplify the error betweem the referenced and desired position.
'''
import pyb

class ProportionalController:
    # This class impements an Proportional Controller 
    # for the ME 405 board.
    
    def __init__(self, k_p):
        ''' Creates a proportional controller object by initializing 
        pins.
        @param kp      A numerical value representing the proportional gain.'''
        
        self.kp = k_p
        
        print('Creating a Proportional Controller with K_p = ', k_p)
        
                                                       
    def update(self, ref, meas):
        ''' Updates the controller with the reference and measured values.
        Returns the error.
        @param ref     Reference value for the motor position.
        @param meas    Measured motor position'''
        
        
        # Returns the actuation value (kp*error)
        return self.kp * (ref - meas)
    
    