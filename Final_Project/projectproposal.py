'''
@page projectproposal Term Project Proposal

@section pstate Problem Statement
Two wheeled transport vehicles have a hard time balancing on their own, especially when the two wheels are parallel to each other.
This project will attempt to create a closed loop algorithm to allow balancing of the vehicle using only the motors.

@section req Requirements
- The vehicle must remain steady without outside assistance.
- The vehicle must counter tipping up to 15 degrees from outside forces.
- The vehicle must not spin or otherwise move more than a few inches when stationary.

@section mat Materials
I anticipate needing only some LEGO pieces, household tools, adhesives, and the ME 405 kit.

@section mats Material Access
I have access to all the materials I anticipate needing for this project.

@section bom Bill Of Materials
- None as of now.

@section safe Safety Assessment
I will have to make sure:
- I don't hurt myself if I use a soldering iron.
- I don't step on a LEGO, which could cause great pain.
- the device can easily be switched off to prevent hair, fingers, or other body parts from being tangled in gearing.

@section time Timeline
- Week 7: Project Proposal
- Week 8: Basic physical protoype, begin working on cod
- Week 9: Code done, physical prototype functional
- Week 10 (Finals week): Physical model fully functional, code fully documented and uploaded.
'''