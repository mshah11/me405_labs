## @file IMUDriver.py
#
# @package IMUDriver
# This file contains code that allows interface with the BNO055 IMU.
# Link to the source code:

# @author Mihir Shah
# @copyright 2020
# @date 5/20/2020
import pyb
import utime


class IMUDriver:
    ## This class implements an IMUDriver for the ME 405 board
    #
    def __init__(self, addy):
        print('Creating an IMU Driver')
        ''' Creates an IMUDriver objectby declaring important addresses and initializing I2C protocol'''
        self.IMU_addy = addy
        self.i2c = pyb.I2C(1,pyb.I2C.MASTER)
        
        self.PWR_MODE_addy = 0x3e
        self.OPR_MODE_addy = 0x3d
        self.UNIT_SEL_addy = 0x3b
        self.SYS_STAT_addy = 0x39
        self.mode = 0
        
        self.ACC_Z_LSB_addy = 0x2c
        self.ACC_Y_LSB_addy = 0x2a
        self.ACC_X_LSB_addy = 0x28
        
        
        # self.EUL_PITCH_MSB_addy = 0x1f
        self.EUL_PITCH_LSB_addy = 0x1e
        
        # self.EUL_ROLL_MSB_addy = 0x1d
        self.EUL_ROLL_LSB_addy = 0x1c
        
        # self.EUL_HEAD_MSB_addy = 0x1b
        self.EUL_HEAD_LSB_addy = 0x1a
        
        
        default_units = 0b10000
        self.i2c.mem_write(default_units, self.IMU_addy, self.UNIT_SEL_addy)


    def enable(self):
        ## This method enables the IMU
        self.i2c.mem_write(1, self.IMU_addy, self.PWR_MODE_addy)
        # self.isEnabled = 1
        
    def disable(self):
        ## This method put the IMU on sleep mode
        self.i2c.mem_write(3, self.IMU_addy, self.PWR_MODE_addy)
        # self.isEnabled = 0
        
    def get_mode(self):
        ## This method returns the mode of the IMU
        return mem.read(1, self.IMU_addy, self.OPR_MODE_addy)
    
    def set_mode(self, mode):
        ## This method allows the user to set the mode of the IMU (1-12)
        # @param mode an integer representing 1 of 12 possible IMU modes
        if 0 <= mode <= 12:
            self.mode = mode
            self.i2c.mem_write(self.mode, self.IMU_addy, self.OPR_MODE_addy)
        
        else:
            print('Invalid mode input.')
            
    def get_cal_status(self):
        ## This method returns the status of the calibration of the system.
        code = self.i2c.mem_read(1, self.IMU_addy, self.SYS_STAT_addy)
        status = ''
        if code == 0b01:
            status += '\nSystem idle'
        if code == 0b10:
            status += '\nSystem error'
        if code == 0b100:
            status += '\nInitializing peripherals'
        if code == 0b1000:
            status += '\nSystem Initialization'
        if code == 0b10000:
            status += '\nExecuting selftest'
        if code == 0b100000:
            status += '\nSensor fusion algorithm running'
        if code == 0b1000000:
            status += '\nSystem running without fusion algorithm'
        
        #if status == '':
         #   status = 'No status code found'
        
        return code
        
    def get_Euler_tuple(self):
        ## This method finds and returns the Euler angles for pitch, roll, and heading.
        
        # Charlie's algorithm to combine LSB and MSB from little endian data
        pitchdata = self.i2c.mem_read(2, self.IMU_addy, self.EUL_PITCH_LSB_addy)     # read two bytes representing the LSB and MSB 
        pitch = pitchdata[1] << 8 | pitchdata[0]                           # combine LSB and MSB into a 16 bit integer
        if (pitch > 32767):
            pitch -= 65536
        
        rolldata = self.i2c.mem_read(2, self.IMU_addy, self.EUL_ROLL_LSB_addy)
        roll = rolldata[1] << 8 | rolldata[0]                              # combine LSB and MSB into a 16 bit integer
        if (roll > 32767):
            roll -= 65536 
        
        headingdata = self.i2c.mem_read(2, self.IMU_addy, self.EUL_HEAD_LSB_addy)
        heading = headingdata[1] << 8 | headingdata[0]                     # combine LSB and MSB into a 16 bit integer
        if (heading > 32767):
            heading -= 65536
            
        pitch = pitch/16
        # divide values by 16 (16 LSB/degree)
        roll = roll/16
        heading = heading/16
            
        self.eulertuple = (pitch, roll, heading)
        return self.eulertuple
    
    def get_Accel_tuple(self):
        ## This method finds and returns the acceleration values for each axis (x, y, z)
        xdata = self.i2c.mem_read(2, self.IMU_addy, self.ACC_X_LSB_addy)
        xval = xdata[1] << 8 | xdata[0]
        if (xval > 32767):
            xval -= 65536
            
        ydata = self.i2c.mem_read(2, self.IMU_addy, self.ACC_Y_LSB_addy)
        yval = ydata[1] << 8 | ydata[0]
        if (yval > 32767):
            yval -= 65536
            
        zdata = self.i2c.mem_read(2, self.IMU_addy, self.ACC_Z_LSB_addy)
        zval = zdata[1] << 8 | zdata[0]
        if (zval > 32767):
            zval -= 65536
            
        xval = xval/100
        # divide the values by 100 (100 LSB/m/s^2)
        yval = yval/100
        zval = zval/100
        
        self.acc_tuple = (xval, yval, zval)
        return self.acc_tuple
    
    
    
if __name__ == '__main__':
    boi = IMUDriver(40)
    boi.enable()
    boi.set_mode(0b1100)
    print(boi.get_cal_status())
    i = 100
    while i > 0:
        utime.sleep_ms(100)
        
        print('Euler: ', boi.get_Euler_tuple())
        i -= 1
    
    
