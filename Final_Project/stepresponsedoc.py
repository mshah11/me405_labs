'''
@page stepresponsedoc Step Response Test Process

@section Description
This showcases the step response testing process to tune the Kp for this controller.
I had 2 goals in tuning this controller:
    First, to have minimal overshoot.
    Second, to have the quickest response time.
    
In order to tune this controller I started with a Kp of 1, then adjusted from there.

@section Plots

@image html step1.png

This was the first step response plot. I used a Kp of 1 and setpoint of 500 and 
did not know what to expect. These were the results:
    
Setpoint =  500
Final Position =  497
Error =  3
Error % =  0.6

There was plenty of overshoot so for the next trial I 
reduced the Kp.

@image html step2.png

This was the second step response plot. I used a Kp of 0.7 and setpoint of 500.
These were the results:
Setpoint =  500
Final Position =  494
Error =  6
Error % =  1.2

Since there was still overshoot I drastically reduced the Kp to 0.1 for Trial 3

@image html step3.png

Finally I achieved a step response with no overshoot. These were the results:
Setpoint =  500
Final Position =  457
Error =  43
Error % =  8.6

Something to note: The final position always ends up with a small error. This 
is due to the stiction required for the motor to start spinning. With thise controller,
the actuation value becomes too small to actually turn the motor once the error is very
small. Thus, the final position has a small error compared to the desired value.    
'''