## @file        mainpage.py
#
# @author       Mihir Shah
#
# @mainpage
#
# @section      sec_main_intro Introduction
#               This landing page is for documentation of code Mihir Shah has generated for ME 405.
#
# @section      sec_main_classes Class Files
#               - @ref MotorDriver "Motor Driver"
#               - @ref EncoderDriver "Encoder Driver"
#               - @ref ProportionalController "Proportional Controller"
#               - @ref IntegralController "Integral Controller"
#               - @ref IMUDriver "IMU Driver"
#
# @section      sec_main_tests Testing Reports
#               - @ref stepresponsedoc "Step Response Test Report"
#               - @ref FinalProjectTest "Term Project Report"
# 
# @section      sec_main_project Term Project
#               - @ref projectproposal "Project Proposal"
#               - @ref FinalProjectTest "Term Project Report"
#               - @ref finalprojectscript "Term Project Script"
#
# @section      sec_main_code Code
#               All source code can be found here: https://bitbucket.org/mshah11/me405_labs/src/master/
#
# @author       Mihir Shah
# @copyright    Mihir Shah 2020
# @date         June 12, 2020  
