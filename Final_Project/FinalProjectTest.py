'''

@page       FinalProjectTest Term Project Report

@section    sec_fin_intro Introduction
            The goal of this project was to build a Balance Bot: a vehicle that could balance on 2 wheels, like a Segway.
            I used the motors and gyroscope from our kit, along with some 3-D printed components and LEGO pieces to build
            the Balance Bot. Please refer to the images below to see what the final product looks like.
            
@image      html bb1.png width=600px
@image      html bb2.png width=600px

@section    sec_fin_proc Process
            I first built the Balance Bot and then set to work on the code. The theory was simple, I would use a simple closed loop control loop,
            similar to what I had been learning about in my ME 422 Controls class. I implemented a PI (Proportional Integral) control loop.
            A block diagram model is shown below. See \ref finalprojectscript for an explanation of the code.
            
            @image html bdiagram.png 
            
@section    sec_fin_vid What Went Right? What Went Wrong? And a Video!
            The LEGO components worked perfectly with the ME405 kit components. I was able to effectively transmit the motor torque
            through my custom printed wheel adapters for the motor D-shaft. I was also able to fit the Nucleo on top of the Balance Bot.
            Unfortunately I was unable to completely dial in the gain values. This could be
            due to a few reasons. First, the cables still attached to the Balance Bot exert weird forces that are difficult
            to compensate for. Given more time and resources I would have liked to have all components and power sources
            onboard the bot. Second, I went about this project in a trial-and-error manner. It may have been more wise to 
            do some hand calculations on the forces occurring in this system in order to generate a transfer function. 
            Then I could have applied my ME 422 Controls knowledge and found some suitable gain ranges, as well as graph 
            some simulated step responses using Matlab and Simulink. However, having just completed that class I am \
            exhausted from controls theory. Please enjoy the short video showing the performance of the bot linked below.
            
            https://cpslo-my.sharepoint.com/:v:/g/personal/mshah11_calpoly_edu/ETmwPjp_DXdMj8cgNYAGEiQB-P9u62e4TajrDQF10bJRTw?e=wOzcch
            
            
            Bonus Blooper: https://cpslo-my.sharepoint.com/:v:/g/personal/mshah11_calpoly_edu/ESZW1JWxMPJAkoWINwl0C8wB5wlGIaQZaWUg8UAWVSSrVA?e=mqFkV9
            
            To view source code for this project please visit: https://bitbucket.org/mshah11/me405_labs/src/master/Final_Project/
                
            '''