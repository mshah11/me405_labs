## @file        finalprojectscript.py



import pyb
from motor import MotorDriver
from encoder import EncoderDriver
from imu import IMUDriver
from proportionalcontroller import ProportionalController
from integralcontroller import IntegralController
import utime


if __name__ == '__main__':
    
    # Create the pin objects used for interfacing with the motor driver
    pin_ENa = pyb.Pin(pyb.Pin.cpu.A10, pyb.Pin.OUT_PP)
    pin_IN1a = pyb.Pin(pyb.Pin.cpu.B4)
    pin_IN2a = pyb.Pin(pyb.Pin.cpu.B5)
    
    pin_ENb = pyb.Pin(pyb.Pin.cpu.C1, pyb.Pin.OUT_PP)
    pin_IN1b = pyb.Pin(pyb.Pin.cpu.A0)
    pin_IN2b = pyb.Pin(pyb.Pin.cpu.A1)
    # Create the timer object used for PWM generation
    tima = pyb.Timer(3, freq=20000)
    timb = pyb.Timer(5, freq=20000)
    # Create a motor object passing in the pins and timer
    moea = MotorDriver(pin_ENa, pin_IN1a, pin_IN2a, tima)
    moeb = MotorDriver(pin_ENb, pin_IN1b, pin_IN2b, timb)
    # Enable the motor driver
    moea.enable()
    moeb.enable()
    
    boi = IMUDriver(40)
    boi.enable()
    boi.set_mode(0b1100)
    
    proppy = ProportionalController(.5)
    inty = IntegralController(0)
    
    
    i = 1000
    while i > 0:
        #utime.sleep_ms(1)
        bro = boi.get_Euler_tuple()
        meas_deg = bro[1]
        ref = 0
            
        Prop_err = proppy.update(ref, meas_deg)
        Integ_err = inty.update(ref, meas_deg)
        
        
        dootie = -int(Prop_err + Integ_err)
        
        # removing motor deadband
        if dootie > 1:
            dootie+=15
        elif dootie < -1:
            dootie-=15
        else:
            dootie = 0   
        moea.set_duty(dootie)
        moeb.set_duty(dootie)
        
        i -= 1
        
    moea.set_duty(0) 
    moeb.set_duty(0)