## @file main.py
# This program implements an EncoderDriver class and tests it.
# Link to the source code: 

#
# @author Mihir Shah
#
# @copyright Mihir Shah, 2020
#
# @date 5/6/2020
#
#

import pyb

class EncoderDriver:
    # This class impements an encoder driver
    # for the ME 405 board.
    
    def __init__(self, enc_a, enc_b, timer):
        ''' Creates an encoder driver object by initializing 
        pins.
        @param enc_a   A pyb.Pin object to use as Encoder A Input to PCB.
        @param enc_b   A pyb.Pin object to use as Encoder B Input to PCB.
        @param timer   A pyb.Timer object to use for tick counting on 
                       both encoders.'''
                       
        print('Creating an Encoder Driver.')
        self.enc_a = enc_a
        self.enc_b = enc_b
        self.timer = timer
        self.tch1 = self.timer.channel(1, pyb.Timer.ENC_A, pin = self.enc_a )
        self.tch2 = self.timer.channel(2, pyb.Timer.ENC_B, pin = self.enc_b )
        self.position = 0
        self.recent_pos_1 = 0
        self.recent_pos_2 = 0
        self.delta = 0
        
        
    def update(self):
        ''' Updates the position of the motor, accounting for under- and over-flow errors'''
        
        # update recent position slot 2
        self.recent_pos_2 = self.recent_pos_1
        # update recent position slot 1
        self.recent_pos_1 = self.timer.counter()
        # calculate delta
        self.delta = self.recent_pos_1 - self.recent_pos_2
        
        # check to see if delta is incorrect (overflow or underflow)
        if self.delta > 32768:
            # fix underflow
            self.under = 65535-self.recent_pos_1
            self.delta = -(self.recent_pos_2 + self.under)
        elif self.delta < -32768:
            # fix overflow
            self.over = self.recent_pos_1
            self.delta = (65535 - self.recent_pos_2 + self.over)
            
        # always update position using delta
        self.position += self.delta
            
        
    def get_position(self):
        ''' Allows user to get the most recent position of the motor (at last update())'''
        return self.position
    
    def set_position(self, pos):
        ''' Allows user to set the position of the motor by inputting a value'''
        self.recent_pos_2 = 0
        self.recent_pos_1 = 0
        self.position = pos
        
    def get_delta(self):
        ''' Allows user to get the most recent delta of the motor (at last update())'''
        return self.delta
    
