## @file mainpage.py

#  @mainpage
#
#  @section sec_intro Introduction
#  This is Mihir Shah's ME 405 Project Documentation.
#
#  @section sec_mot Motor Driver
#  Some information about the motor driver with links. Please see motor.Motor which is part of the \ref motor package.
#
#  @section sec_enc Encoder Driver
#  Some information about the encoder driver with links. Please see encoder.Encoder which is part of the \ref encoder package.
#
#  @author Mihir Shah
#
#  @copyright Mihir Shah 2020
#
#  @date May 6, 2020
#