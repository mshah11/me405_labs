''' @file mainpage.py

@author Mihir Shah

@mainpage

@section intro Introduction
This landing page is for documentation of code Mihir Shah has generated for ME 405.

@section classes Class Files
\li motor
\li encoder
\li proportionalcontroller

@section tests User Test Files
\li StepResponseTest
\li StepResponseDoc

@section Code
\li Lab source can be found here: https://bitbucket.org/mshah11/me405_labs/src/master/

@author Mihir Shah
@copyright Mihir Shah 2020
@date May 14, 2020  '''