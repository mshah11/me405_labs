'''@file StepResponseTest.py

@package StepResponseTest

@subsection Purpose
This code implements a step response test for the proportional controller.

@subsection Background


'''
import pyb
from motor import MotorDriver
from encoder import EncoderDriver
from proportionalcontroller import ProportionalController
import utime

## Step response test code.
#  This code creates MotorDriver, EncoderDriver, and ProportionalController objects
#  and allows for user to input kp, setpoint, and number of control loop iterations.
#  It then performs a step response test of the motor and outputs the position vs. time.
#  These values can be copy + pasted into a spreadsheet and graphed to obtain the response curve.
#  The code will loop until a key other than "Y" is pressed, allowing the user to run multiple tests.

if __name__ == '__main__':
    
    print('Welcome to the motor step response tester.')
    x = input('Would you like to perform a step response test? (Y/N)' )
    while x == 'Y' or x == 'y':
        
        
        # Create the pin objects used for interfacing with the motor driver
        pin_EN = pyb.Pin(pyb.Pin.cpu.A10, pyb.Pin.OUT_PP)
        pin_IN1 = pyb.Pin(pyb.Pin.cpu.B4)
        pin_IN2 = pyb.Pin(pyb.Pin.cpu.B5)
        
        # Create the timer object used for PWM generation
        tim = pyb.Timer(3, freq=20000)
        
        # Create a motor object passing in the pins and timer
        moe = MotorDriver(pin_EN, pin_IN1, pin_IN2, tim)
        # Enable the motor driver
        moe.enable()
        moe.set_duty(0)
        
        # Create an EncoderDriver object passing in pins and timer
        encoe = EncoderDriver(pyb.Pin.cpu.B6, pyb.Pin.cpu.B7, pyb.Timer(4, prescaler=0,period = 65535))
        
        # Allow user to input kp value
        kp = float(input('Enter kp value: '))
        
        # Create a ProportionalController object passing in kp value 
        controe = ProportionalController(kp)
        
        # Allow user to input setpoint value
        setpt = int(input('Enter setpoint: '))
        
        # Create time and position lists.
        time = []
        pos = []
        
        # Update encoder object to find the current position of the motor.
        encoe.update()
        
        # Set the initial actuation signal with curremt position and setpoint value.
        act_signal = controe.update(encoe.get_position(), setpt)
        
        # Allow user to input the number of controller loops. (Duration of step test)
        num_loops = int(input('Enter number of controller loops: '))
        
        # Set while loop variable to 0
        i = 0
        
        # This is the propotional control loop. It checks the current motor 
        # position and sends the motor an actuation signal based on the control 
        # loop calculation
        while i < num_loops:
            
            # Runs every ~10ms
            utime.sleep_ms(10)
            
            # Update encoder
            encoe.update()
            
            # Determine the new actuation PWM signal
            act_signal = controe.update(setpt, encoe.get_position())
            
            # Set new duty cycle with actuation signal rounded to an integer
            moe.set_duty(int(act_signal))
            
            # Add the current time count to the time list
            time.append(utime.ticks_ms())
            
            # Add the current position to the position list
            pos.append(encoe.get_position())
            
            # Increment loop counter
            i += 1
            
        # Stop the motor
        moe.set_duty(0)
        
        # Determine length of position list
        N = len(pos)  
        
        # Print the results of the step response test
        print('Step Response Test Results')
        print('Time, Position')
        
        # In two columns for easy conversion to spreadsheet format
        for n in range(N):
            print('{:},{:}'.format(utime.ticks_diff(time[n],time[0]),pos[n]))
            
        # Print a variety of step response parameters for user to view
        print('(kp = ', kp,')')
        print('Setpoint = ', setpt)
        encoe.update()
        print('Final Position = ',encoe.get_position())
        print('Error = ', setpt - encoe.get_position())
        print('Error % = ', 100*(setpt - encoe.get_position())/setpt)
        
        # Ask user if they would like to perform the test again. Loop if 'Y'
        x = input('Would you like to perform a step response test? (Y/N)' )
        
    print('Program Terminated. Thank you.')